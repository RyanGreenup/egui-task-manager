#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release

use eframe;
use eframe::egui;
use eframe::egui::SidePanel;
use egui_commonmark::*;
use open;
use task_manager_sqlite;
fn main() {
    run();
}

fn run() {
    // Log to stdout (if you run with `RUST_LOG=debug`).
    tracing_subscriber::fmt::init();

    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(320.0, 240.0)),
        ..Default::default()
    };
    eframe::run_native(
        "My egui App",
        options,
        Box::new(|_cc| Box::new(MyApp::default())),
    );
}

struct MyApp {
    name: String,
    age: u32,
    side_panel: bool,
    tasks: Vec<task_manager_sqlite::Task>,
    current_content: String,
    files: Vec<String>,
    current_file: String,
    light_theme: bool,
}

impl Default for MyApp {
    fn default() -> Self {
        Self {
            name: "".to_owned(),
            age: 42,
            files: vec![],
            side_panel: true,
            tasks: task_manager_sqlite::get_tasks().expect("Unable to get tasks from database"),
            current_content: "".to_string(),
            current_file: "".to_string(),
            light_theme: false,
        }
    }
}

impl MyApp {
    fn show_side_panel(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        if self.side_panel {
            let side_panel = SidePanel::left("side_panel");
            side_panel.show(ctx, |ui| {
                ui.heading("Tasks");
                ui.label("All tasks");
                //button to toggle the side panel
                if ui.button("Toggle side panel").clicked() {
                    self.side_panel = !self.side_panel;
                }

                // create a scrollable area
                let scroll_area = egui::ScrollArea::new([true, true]);
                scroll_area.show(ui, |ui| {
                    self.files = task_manager_sqlite::search_files(self.name.as_str()).unwrap();

                    // TODO this could be slow, don't run every frame?
                    let tree = task_manager_sqlite::get_directory_tree(&self.files);
                    // get the keys from tree and order them
                    let mut keys: Vec<_> = tree.keys().collect();
                    keys.sort();

                    for key in keys {
                        // get the last 4 directories of the key
                        let title = key
                            .split('/')
                            .rev()
                            .take(4)
                            .collect::<Vec<&str>>()
                            .join("/");

                        // format the date "20-Fri/03/01-Jan/2023" as "Fri, 20 Jan 2023"
                        let title: Vec<&str> = title.split('/').collect();
                        let title = format!("{} {} {}", title[0], title[2], title[3]);

                        // foldable section
                        ui.collapsing(title, |ui| {
                            for file in tree.get(key).unwrap() {
                                let basename = std::path::Path::new(&file)
                                    .file_name()
                                    .unwrap()
                                    .to_str()
                                    .unwrap();

                                if ui.button(basename.clone()).clicked() {
                                    // TODO this should be a struct or tupple
                                    println!("Clicked on {}", file);
                                    self.current_content = std::fs::read_to_string(&file).unwrap();
                                    self.current_file = file.to_string();
                                }
                            }
                        });
                    }
                });
            });
        }
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        // Add a top panel that can be toggled
        self.show_side_panel(ctx, _frame);

        egui::TopBottomPanel::top("Top Panel").show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.label("Search: ");
                ui.text_edit_singleline(&mut self.name);
                if ui.button("Search").clicked() {
                    self.files = task_manager_sqlite::search_files(self.name.as_str()).unwrap();
                }
                // a button for switching to light mode
                if self.light_theme {
                    if ui.button("Dark").clicked() {
                        ctx.set_visuals(egui::Visuals::dark());
                        self.light_theme = false;
                    }
                } else {
                    if ui.button("Light").clicked() {
                        ctx.set_visuals(egui::Visuals::light());
                        self.light_theme = true;
                    }
                }
            });
        });

        egui::TopBottomPanel::bottom("bottom_panel").show(ctx, |ui| {
            let projects = task_manager_sqlite::get_projects(&self.files);
            let scroll_area = egui::ScrollArea::new([true, true]);
            scroll_area.show(ui, |ui| {
                ui.horizontal(|ui| {
                    ui.label("Projects:");
                    for project in projects {
                        if ui.button(&project).clicked() {
                            self.name =
                                ":Projects/".to_string() + &project.clone() + "| <Alt Query>";
                        }
                    }
                });
            });
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            // a horizontal layout
            ui.horizontal(|ui| {
                // a button to open the current file
                if ui.button("Open file").clicked() {
                    // open the current file in the default text editor
                    let _ = open::that(&self.current_file);
                }
                // How far in the future? TODO
                // ui.add(egui::Slider::new(&mut self.age, 0..=120).text("age"));
            });
            // button to toggle side panel
            if ui.button("Toggle side panel").clicked() {
                self.side_panel = !self.side_panel;
            }

            // Stores image handles between each frame
            let mut cache = CommonMarkCache::default();

            //scrollable
            let scroll_area = egui::ScrollArea::new([true, true]);
            scroll_area.show(ui, |ui| {
                CommonMarkViewer::new("viewer").show(ui, &mut cache, self.current_content.as_str());
            })
        });
    }
}
