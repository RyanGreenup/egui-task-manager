use pulldown_cmark::{html, Options, Parser};
use rusqlite::{ffi::SQLITE_NULL, Connection};

use regex::Regex;

use std::collections::HashMap;

use std::fmt::Display;
use tempfile::NamedTempFile;
use walkdir::WalkDir;
pub struct Task {
    pub id: Option<i32>,
    pub name: String,
    pub description: Option<String>,
    pub scheduled: Option<String>,
    pub deadline: Option<String>,
    pub project: Option<String>,
}

impl Task {
    pub fn new(
        id: Option<i32>,
        name: &str,
        description: Option<&str>,
        scheduled: Option<&str>,
        deadline: Option<&str>,
        project: Option<&str>,
    ) -> Self {
        Self {
            id,
            name: name.to_string(),
            description: description.map(str::to_string),
            scheduled: scheduled.map(str::to_string),
            deadline: deadline.map(str::to_string),
            project: project.map(str::to_string),
        }
    }
}

// Implement display train for Task
impl std::fmt::Display for Task {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Task: {} {} {} {} {}",
            self.name,
            self.description
                .clone()
                .unwrap_or("NULL".to_string())
                .to_string(),
            self.scheduled.clone().unwrap_or("NULL".to_string()),
            self.deadline.clone().unwrap_or("NULL".to_string()),
            self.project.clone().unwrap_or("NULL".to_string()),
        )
    }
}

pub fn add_data(data: Task) {
    let conn = Connection::open("tasks.db").unwrap();

    conn.execute(
        "INSERT INTO tasks (name, description, scheduled, deadline, project)
                  VALUES (?1, ?2, ?3, ?4, ?5)",
        &[
            &data.name,
            &data.description.unwrap_or("NULL".to_string()),
            &data.scheduled.unwrap_or("NULL".to_string()),
            &data.deadline.unwrap_or("NULL".to_string()),
            &data.project.unwrap_or("NULL".to_string()),
        ],
    )
    .unwrap();
}

pub fn create_sqlite_database(db_name: &str) {
    // check if the file extists
    match std::fs::File::open(db_name) {
        Ok(_) => {
            println!("File exists");
            return;
        }
        Err(_) => (),
    };

    let conn = Connection::open(db_name).unwrap();

    conn.execute(
        "CREATE TABLE tasks (
                  id              INTEGER PRIMARY KEY,
                  name            TEXT NOT NULL,
                  description     TEXT,
                  scheduled       DATETIME  NOT NULL,
                  deadline        DATETIME,
                  project         TEXT
                  )",
        [],
    )
    .unwrap();
}

pub fn add_some_tasks() {
    add_data(Task::new(
        None,
        "Write a Task Manager",
        Some("Write a Task Manager in Rust"),
        Some("2022-01-01 12:00:00"),
        Some("2022-01-15 12:00:00"),
        None,
    ));

    add_data(Task::new(
        None,
        "Watch tv",
        Some("Watch tv In the Evening"),
        Some("2022-01-01 12:00:00"),
        Some("2022-01-15 12:00:00"),
        None,
    ))
}

// function to create a Task struct from function in the database
pub fn get_tasks() -> Result<Vec<Task>, rusqlite::Error> {
    let conn = Connection::open("tasks.db").unwrap();

    let mut stmt = conn.prepare("SELECT * FROM tasks").unwrap();
    let task_iter = stmt.query_map((), |row| {
        Ok(Task {
            id: Some(row.get(0)?),
            name: row.get(1)?,
            description: row.get(2)?,
            scheduled: row.get(3)?,
            deadline: row.get(4)?,
            project: row.get(5)?,
        })
    })?;

    let mut tasks: Vec<Task> = Vec::new();
    for task in task_iter {
        tasks.push(task?);
    }

    return Ok(tasks);
}

// function to search through a directory and return the full path of all files that contain a given string
pub fn search_files(search_string: &str) -> Result<Vec<String>, std::io::Error> {
    // make search string lower case
    let search_string = search_string.to_lowercase();

    // Create a regex from the search string
    let re = regex::Regex::new(&search_string).unwrap_or_else(|_| regex::Regex::new("").unwrap());

    let mut files: Vec<String> = Vec::new();
    let home = std::env::var("HOME").unwrap();

    // TODO hard coded paths are bad
    for entry in WalkDir::new(home + "/Agenda/Agenda_Maybe/Scheduled/") {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() {
            let file = std::fs::read_to_string(path)?;

            // test if the file matches the regex
            if re.is_match(&file) || re.is_match(path.to_str().unwrap()) {
                files.push(path.to_str().unwrap().to_string());
            }
        }
    }
    // sort the files

    // TODO Sort should NOT run each time a fraw is redrawn, confirm how often this function is called
    files.sort();
    Ok(files)
}

// function that takes a date and walks through a directory where directories are named after dates and returns files after that date
pub fn get_files_after_date(date: &str) -> Result<Vec<String>, std::io::Error> {
    let mut files: Vec<String> = Vec::new();
    let home = std::env::var("HOME").unwrap();
    // make search string lower case
    let date = date.to_lowercase();

    // TODO hard coded paths are bad
    for entry in WalkDir::new(home + "/Agenda/Agenda_Maybe/Scheduled/") {
        let entry = entry?;
        let path = entry.path();
        if path.is_file() {
            let file = std::fs::read_to_string(path)?;
            if file.to_lowercase().contains(&date) || path.to_str().unwrap().contains(&date) {
                files.push(path.to_str().unwrap().to_string());
            }
        }
    }
    Ok(files)
}

// A function that takes a vector of strings which are files and searches through them for any line with the word :Projects
// and returns a vector of strings which are the projects
pub fn get_projects(files: &Vec<String>) -> Vec<String> {
    let mut projects: Vec<String> = Vec::new();
    for file in files {
        let file = std::fs::read_to_string(file).unwrap();
        for line in file.lines() {
            if line.contains(":Projects") {
                let project = line.split(":").collect::<Vec<&str>>()[1];
                // let project be everything after the first slash
                let project = project.split("/").collect::<Vec<&str>>()[1..].join("/");
                // add the project to the vector only if it is unique
                if !projects.contains(&project) {
                    projects.push(project.to_string());
                }
                // projects.push(project.to_string());
            }
        }
    }
    projects
}

// A function that takes in a vector of paths and returns a hashmap that contains the nested tree structure of the directories and files
pub fn get_directory_tree(paths: &Vec<String>) -> HashMap<String, Vec<String>> {
    let mut directories: HashMap<String, Vec<String>> = HashMap::new();
    for path in paths {
        let op = path.clone();
        let mut dir = path.split("/").collect::<Vec<&str>>();
        dir.pop();
        let dir = dir.join("/");
        let file = path.split("/").collect::<Vec<&str>>().pop().unwrap();
        if directories.contains_key(&dir) {
            directories.get_mut(&dir).unwrap().push(op.to_string());
        } else {
            directories.insert(dir, vec![op.to_string()]);
        }
    }
    // sort the files
    for (_, files) in directories.iter_mut() {
        files.sort();
    }
    directories
}
